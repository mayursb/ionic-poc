// Set up
var express = require('express');
var app = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var cors = require('cors');

// mongo db connection
// Change this to your public/private IP address to allow the application to work on your mobile
// Also connect your mobile to same network as your PC 
mongoose.connect('127.0.0.1:27017/dummy');

// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

mongoose.connection.on('open', function (ref) {
    console.log('Connected to mongo server.');
    mongoose.connection.db.listCollections().toArray(function (err, names) {
        if (err) {
            console.log(err);
        }
        else {
            names.forEach(function (e, i, a) {
                console.log("--->>", e.name);
            });
        }
    });
})

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' }));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Models
var friendList = mongoose.model('friendList', {
    name: String,
    address: String,
    age: Number
});

var db = mongoose.connection.db;

// Get friends list
app.get('/api/friendslist', function (req, res) {
    console.log("fetching friends list");

    var friendListCol = db.collection('friendList');

    friendListCol.find().toArray(function (err, data) {
        if (err) {
            console.log(err);
            res.send(err)
        }
        else {
            console.log(data);
            res.send(data);
        }
    });
});

// listen (start app with node server.js) ======================================
app.listen(8080);
console.log("App listening on port 8080");