import { TestBed, inject } from '@angular/core/testing';

import { FriendServiceService } from './friend-service.service';

describe('FriendServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FriendServiceService]
    });
  });

  it('should be created', inject([FriendServiceService], (service: FriendServiceService) => {
    expect(service).toBeTruthy();
  }));
});
