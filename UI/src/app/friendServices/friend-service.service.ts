import { Injectable } from '@angular/core';
import { friend } from '../friends/friend';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Http } from '@angular/http';
import { Response } from '@angular/http/src/static_response';
import 'rxjs/add/operator/map';

@Injectable()
export class FriendServiceService {

  friendsList: friend[];
  // Change this to your public/private IP address to allow the application to work on your mobile
  // Also connect your mobile to same network as your PC 
  friendslistUrl = 'http://192.168.0.102:8080/api/friendslist';  // URL to web api

  constructor(
    public http: Http) { }

  getFriendList(): Observable<friend[]> {
    console.log('FriendServiceService : getFriendList() called');
    console.log('Calling API : ' + this.friendslistUrl);

    return this.http.get(this.friendslistUrl)
      .map(res => {
        return res.json().map(item => {
          return new friend(
            item.name,
            item.address,
            item.age
          );
        });
      });
  }
}
