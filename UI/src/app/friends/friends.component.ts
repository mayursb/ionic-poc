import { Component, OnInit } from '@angular/core';
import { friend } from '../friends/friend';
import { FriendServiceService } from '../friendServices/friend-service.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html'
})

export class FriendsComponent implements OnInit {

  friendsList: friend[];

  constructor(private friendServiceService: FriendServiceService) { }

  ngOnInit() {
    console.log('FriendsComponent : ngOnInit() started');

    this.friendServiceService
      .getFriendList()
      .subscribe(friendsList => this.friendsList = friendsList);

    console.log('FriendsComponent : ngOnInit() ended');
  }
}
